﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ClaseMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)

        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            
         routes.MapRoute(
                name: "Deporteruta2",
                url: "deportes/deporte/{categoria}",
                defaults: new { 
                    Controller = "Deportes",
                    action = "index",
                     categoria = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional 
         
                
                }
            );
        }
    }
}